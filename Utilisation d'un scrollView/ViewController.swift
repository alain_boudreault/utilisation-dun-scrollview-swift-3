//
//  ViewController.swift
//  Utilisation d'un scrollView
//
//  Created by Alain on 16-11-28.
//  Copyright © 2016 Alain. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var unScrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        unScrollView.contentSize = CGSize(width:340, height:2000)
        let uneTresGrandeScène =    UINib(nibName: "big", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        unScrollView.addSubview(uneTresGrandeScène)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

